<div align="center"><h1><ins>DevOps Demo Project:</ins></h1></div>
<div align="center"><h2> Automate Kubernetes Deployment</h2></div>

<div align="center">![1](img/devOps.png)</div>
<br>

<ins>DevOps Tools used in this project</ins>:<br>- Ansible<br>- Terraform<br>- Kubernetes<br>- AWS: EKS, VPC, and EC2<br>- Git<br>- GitLab<br>- Microsoft Visual Studio Code<br>- Python<br>- Windows PowerShell

<ins>Operating Systems</ins>:<br>- Windows 11 with WSL<br>- Linux

<ins>Web Server</ins>: nginx

<ins>Project Purpose</ins>: Use Terraform to automate the creation of an AWS EKS cluster. Use Ansible to automatically deploy an ngnix application to the EKS cluster in the my-app namespace.

<ins>Out of Scope</ins>:<br>- Git installation in Windows<br>- GitLab account creation<br>- AWS account creation<br>- AWS IAM Access Key creation<br>- kubectl installation

# Project Walkthrough
I. [Preliminary Project Tasks](#prelim)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A) [Install Terraform on Windows](#install_tf)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B) [Project Creation](#project)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C) [Terraform Configuration Files](#tf_config)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i) [vpc.tf and terraform.tfvars](#vpc)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ii) [eks-cluster.tf](#eks)<br>
II. [Terraform: EKS Cluster Creation](#eks_cluster_creation)<br>
III. [Create EKS Cluster Namespace](#namespace)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A) [Ansible Prerequisites:](#ansible_prereq)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i) [Windows 11 WSL Setup](#wsl)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ii) [Python3: jsonpatch, PyYAML, & k8s](#python)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iii) [aws-iam-authenticator](#aws_iam)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iv) [AWS CLI and aws configure](#awscli)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;v) [Install Ansible in WSL](#install_ansible)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B) [kubeconfig](#kubeconfig)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C) [Ansible:](#ansible)<br> 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i) [ansible.cfg and hosts](#ansiblecfg_hosts)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ii) [deploy-to-k8s.yaml: Create Namespace](#deploy_to_k8s)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iii) [Execute Playbook & Confirm Namespace](#execute1)<br>
IV. [Deploy nginx Application in my-app Namespace](#nginx)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A) [nginx Kubernetes Configuration](#nginx_k8s)<br> 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B) [Execute Ansible Playbook & Confirm nginx](#execute2)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C) [Optimization: kubeconfig](#optimize)<br>
V. [Project Teardown](#teardown)


## I. Preliminary Project Tasks <a name="prelim">
Prior to starting this project, a few prerequisites must be satisfied. First, Terraform needs to be installed on Windows. Second, the project will be created in Microsoft Visual Studio Code locally and pushed to a GitLab code repository remotely. Last, two Terraform configuration files will be created – one for the VPC and one for the EKS cluster. Since this is a local setup, the variables will be configured inside the terraform.tfvars file.
## A) Install Terraform on Windows <a name="install_tf">
While it is possible to use Chocolatey as a package manager to install Terraform in Windows, the Chocolatey method is not maintained by HashiCorp. Therefore, instead, the binary from the official Terraform site will be used.
1.	Navigate to the official Terraform website located here: (https://developer.hashicorp.com/terraform/downloads).  

2.	On the Terraform website, click on the Windows tab. This project assumes a AMD64 architecture, but if the engineer's workstation is using ARM64, select that download option instead. Click the Download link/button next to the AMD64 option.

    ![1pit](img/prelim/install_tf/1.png)

3.	Minimize the browser and open File Explorer. Navigate to the Downloads folder. Right-click the terraform_1.6.2_windows_amd64.zip archive folder > Extract All… 
  
    ![2pit](img/prelim/install_tf/2.png)

4.	On the Extract Copressed (Zipped) Folders dialog box, accept the defaults to extract to the same location in the Downloads folder. Click the <b>Extract</b> button.

    ![3pit](img/prelim/install_tf/3.png)

5.	The extracted folder will launch a new File Explorer window with the Terraform executable. 

    ![4pit](img/prelim/install_tf/4.png) 

6.	Navigate up one directory back to the Downloads folder. Right-click on the unzipped terraform_1.6.2_windows_amd64 folder > Copy as path. 
 
    ![5pit](img/prelim/install_tf/5.png)

7.	In Windows Search, enter “env” as a search criterion. Click “Edit the system environment variables.”
 
    ![6pit](img/prelim/install_tf/6.png)

8.	On the System Properties dialog box, click the <b>Environment Variables</b> button.

    ![7pit](img/prelim/install_tf/7.png)

9.	The Environment Variables dialog box will then open. In the System variables section, highlight Path and click the <b>Edit</b> button.

    ![8pit](img/prelim/install_tf/8.png) 

10.	In the Edit environment variable dialog box, click the New button and paste the path to the Terraform executable. Click OK 3x to close out of the System Properties dialog box.

    ![9pit](img/prelim/install_tf/9.png) 

11.	<b>Close out of all open PowerShell windows.</b> Open a fresh PowerShell window and enter the following command to test the availability of the terraform command.

        terraform -v

Terraform is now readily available on the Windows 11 system.

## B) Project Creation <a name="project"> 
The purpose of this section is to create a project locally in Microsoft Visual Studio Code and push it to a remote GitLab repository.

1.	Open File Explorer. Navigate to the root of C:\ and create a folder called automate-k8s-eks-tf-ansible.

    ![1pp](img/prelim/project/1.png)  

2.	Open Microsoft Visual Studio Code. Click on File > Open Folder...

    ![2pp](img/prelim/project/2.png)  
 
3.	Browse out to the root of C:\ and select the automate-k8s-eks-tf-ansible folder. Click the <b>Select Folder</b> button.

    ![3pp](img/prelim/project/3.png)   

4.	When prompted, click the <b>Yes, I trust the authors</b> button.

    ![4pp](img/prelim/project/4.png)   

5.	The project folder will now appear in the left pane.

    ![5pp](img/prelim/project/5.png)   

6.	Under the root of the project folder, create a blank README.md file.
 
    ![6pp](img/prelim/project/6.png)  

7.	Minimize Microsoft Visual Studio Code. Open a web browser and login to GitLab. Click the <b>New Project</b> button.

    ![7pp](img/prelim/project/7.png)   

8.	On the Create new project screen, click Create blank project. 

    ![8pp](img/prelim/project/8.png)   

9.	On the Create blank project screen, name the project “automate-k8s-eks-tf-ansible.” In the Project URL section, specify the username from the dropdown (e.g. jlomba09), and ensure the project slug matches the project name. Set the visibility level to Public and uncheck the box to initialize the repository with a README. Click the <b>Create project</b> button.

    ![9pp](img/prelim/project/9.png)   

10.	Maximize Microsoft Visual Studio Code. In the top left, click the Hamburger Menu (≡) > Terminal > New Terminal.

    ![10pp](img/prelim/project/10.png)   

11.	<ins>Note</ins>: Although the installation of Git on Windows 11 is outside the scope of this project walkthrough, the Git installer can be found here: https://git-scm.com/download/win

    In the bottom terminal pane, initialize the git repository with the following command:

        git init

    The output will show the repository is initialized.
 
    ![11pp](img/prelim/project/11.png)   

12.	Set the remote origin as the newly created project in GitLab. Substitute the username (e.g. jlomba09) with the appropriate username.

        git remote add origin git@gitlab.com:jlomba09/automate-k8s-eks-tf-ansible.git


13.	Add changes and set an initial commit message:

        git add .
        git commit -m "Initial commit"

14.	Configure the push behavior for the master branch with the following command:

        git push -u origin master

15.	In the web browser, refresh the automate-k8s-eks-tf-ansible project in GitLab and confirm the blank README file exists:

    ![12pp](img/prelim/project/12.png)   

Terraform configuration files can now be added to the project.

## C) Terraform Configuration Files <a name="tf_config">
This section will create three Terraform configuration files: (1) vpc.tf, (2) eks-cluster.tf, and (3) terraform.tfvars. The vpc.tf configuration file will automatically provision a VPC in AWS. The eks-cluster.tf configuration file will automatically provision an EKS cluster in AWS. The terraform.tfvars file will store variable values locally on the workstation.

1.	Restore Microsoft Visual Studio Code. At the root of the project folder, begin by clicking the new folder icon and adding a folder named 'terraform.'
 
    ![1ptc](img/prelim/tf_config/1.png)   

2.	Create 3 new files under the terraform folder by clicking the new file icon. Name the files (1) vpc.tf, (2) eks-cluster.tf, and (3) terraform.tfvars.

    ![2ptc](img/prelim/tf_config/2.png)  
 
Each of these Terraform configuration files will be discussed next.

## i) vpc.tf and terraform.tfvars <a name="vpc">
In this section, the vpc.tf Terraform configuration file will be created to automatically provision an AWS VPC. In conjunction, the terraform.tfvars file will be create to store local variable values.

1.	Navigate to the official Terraform website for utilizing the vpc module (https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/latest). In the Provision Instructions section, copy the module code to the clipboard.

2. Restore Microsoft Visual Studio Code and paste the module code into vpc.tf.

        module "vpc" {
          source  = "terraform-aws-modules/vpc/aws"
          version = "5.1.2"
        }

3.	Provide a unique name to the module by changing the name from “vpc” to “myapp-vpc.”

        module "myapp-vpc" {
        }

4.	In the module block, enter a couple carriage returns. At the same indentation level as the version attribute, specify the name attribute as “myapp-vpc.” 

        name = "myapp-vpc"

    <ins>Note:</ins> When the Terraform configuration is later applied, myapp-vpc will appear in the Name column of the AWS VPC management page.  
 
    ![1ppv](img/prelim/tf_config/vpc/1.png)  

5.	For security reasons, IP addresses will not be hardcoded in the Terraform configuration files, but instead, parameterized and stored in the local terraform.tfvars file. Inside the “myapp-vpc” module block, under the name attribute, parameterize the CIDR block by using the var prefix followed by the variable name as shown below:

        cidr = var.vpc_cidr_block

    Declare the vpc_cidr_block variable at the top of the vpc.tf file:

        variable vpc_cidr_bock {}

6.	For high availability, a single private subnet and a single public subnet will be allocated per availability zone. As there are 3 availability zones in the US-East-2 region, there will be a total of 6 subnets (3 private, 3 public).  To handle multiple subnet values, the values for the private_subnets and public_subnets attributes will later be specified as arrays in the terraform.tfvars file. 

    Inside the “myapp-vpc” module block, under the CIDR attribute, parameterize the private_subnets and public_subnets attributes as done in step 5:

        private_subnets = var.private_subnet_cidr_blocks
        public_subnets = var.public_subnet_cidr_blocks

    Declare the private_subnets and public_subnets as variables at the top of the vpc.tf file:

        variable private_subnet_cidr_blocks {}
        variable public_subnet_cidr_blocks {}

7.	Next, copy the 3 variables at the top of the vpc.tf file and paste them into the terraform.tfvars file. Remove “variable” from each line and replace the curvy brackets with an equals sign as shown below:

        vpc_cidr_block =
        private_subnet_cidr_blocks =
        public_subnet_cidr_blocks = 

8.	In terraform.tfvars, assign the vpc_cidr_block variable the value of 10.0.0.0/16. Enclose the value in double quotes.

        vpc_cidr_block = "10.0.0.0/16"

9.	Recall from Step 6 that there will be 3 private and 3 public subnets. In practice, a subnet calculator can be used to divide up a larger subnet (https://www.davidc.net/sites/default/subnets/subnets.html). If the 10.0.0.0/16 block is divvied up into CIDR blocks with a /24 mask, notice only the third octet of the network address changes:
 
    ![2ppv](img/prelim/tf_config/vpc/2.png)  

    In terraform.tfvars, under the vpc_cidr_block variable, specify the private and public subnet CIDR block variables with values as arrays (denoted by the square brackets). Increment the third octet in each subnet by one. 

        private_subnet_cidr_blocks = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
        public_subnet_cidr_blocks = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]

10.	The next attribute that needs to be accounted for in the “myapp-vpc” module block is the availability zones (azs) of the VPC.

    The availability zones for the US-East-2 region can be determined by opening PowerShell and entering the following command:

        aws ec2 describe-availability-zones --region us-east-2

    The ZoneName attribute will identify the names and number of availability zones in the US-East-2 region.

    ![3ppv](img/prelim/tf_config/vpc/3.png)   

    However, hardcoding the availability zones in the Terraform configuration file does not provide much in terms of flexibility for different regions, as the number and names of the availability zones will vary from region to region. To increase flexibility and dynamically query AWS for the number and names of AZs regardless of region, the aws_availability_zone data source will instead be used(https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones). In vpc.tf, under the variable declarations, use the aws_availability_zones data source type and name it “azs” without attributes:

        data "aws_availability_zones" "azs" {}

11. Since the “aws_availability_zones” data source uses the AWS provider, the AWS provider must be included in the VPC Terraform configuration file. Navigate to the official Terraform documentation for the AWS provider located here: https://registry.terraform.io/providers/hashicorp/aws/latest.  Click the Use Provider dropdown button and copy the code to the clipboard.

    ![4ppv](img/prelim/tf_config/vpc/4.png)  

12. Paste the provider code at the very top of the vpc.tf configuration file. In the provider block, delete the configuration options comment and add the region attribute with a value of US-East-2 as shown below:

        terraform {
          required_providers {
            aws = {
              source = "hashicorp/aws"
              version = "5.22.0"
            }
          }
        }

        provider "aws" {
          region = “us-east-2”
        }

13. Return to the “myapp-vpc” module block. Under the public_subnets attribute, add a new attribute called azs. Set the azs attribute’s value using the syntax of <i>data.data type.data name.data attribute</i>. Specify the names attribute at the end:

        azs = data.aws_availability_zones.azs.names

    Example usage of the names attribute is shown in the official Terraform documentation here: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones 

14. Revisit the official Terraform documentation on the VPC module: https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/latest. Scroll down to the NAT Gateway Scenarios section. Notice that enable_nat_gateway is set to true, regardless of scenario. While enable_nat_gateway being set to true is the default behavior, for this project, the enable_nat_gateway attribute will explicitly be set to true in the Terraform configuration file. 

    Accordingly, copy the enable_nat_gateway attribute and its true value and paste it underneath the azs attribute in the “myapp-vpc” module of the vpc.tf configuration file.

        enable_nat_gateway = true
 
    ![5ppv](img/prelim/tf_config/vpc/5.png)  

    In the NAT Gateway Scenarios section, also notice the configuration for single NAT gateway. Copy the single_nat_gateway attribute and its associated true value from the website and paste it underneath the “myapp-vpc” module block. Disregard the one_nat_gateway_per_az attribute.

        single_nat_gateway = true
 
    <ins>Note:</ins> In this single NAT gateway configuration, the private subnets associated with the 3 availability zones will use a mutual NAT gateway to route traffic from the Internet.

15. In the “myapp-vpc” module, set the enable_dns_hostnames attribute as true to map the IP addresses to DNS hostnames.

        enable_dns_hostnames = true

16. For steps 16-18, reference the following link: https://repost.aws/knowledge-center/eks-vpc-subnet-discovery. 

    Restore the vpc.tf file and create 3 tag blocks: (1) tags, (2) public_subnet_tags, and (3) private_subnet_tags. For this project, the EKS cluster name will be “myapp-eks-cluster.” Under each tag block, paste the following so the Kubernetes Cloud Controller Manager and AWS Load Balancer Controller can recognize the cluster’s subnets.

        "kubernetes.io/cluster/myapp-eks-cluster" = "shared"

    This is summarized below:

        tags = {
          "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
        }
        public_subnet_tags = { 
          "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
        }
        private_subnet_tags = {
          "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
        }

17. In the public_subnet_tags block, add an additional tag based on the following key-value pair:

    ![6ppv](img/prelim/tf_config/vpc/6.png)   

    The modified public_subnet_tags block should now look like this:

        public_subnet_tags = {
          "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
          "kubernetes.io/role/elb" = 1
        }

18. In the private_subnet_tags block, add an additional tag based on the following key-value pair:

    ![7ppv](img/prelim/tf_config/vpc/7.png)  

    The modified private_subnet_tags block should now look like this:

        private_subnet_tags = {
          "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
          "kubernetes.io/role/internal-elb" = 1
        }

The vpc.tf and terraform.tfvars Terraform configuration files are complete.

## ii) eks-cluster.tf <a name="eks">
This section covers the eks-cluster.tf Terraform configuration file.

1.	Navigate to the official Terraform website for utilizing the eks module (https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/latest). In the Provision Instructions section, copy the module code to the clipboard.

2.	Restore Microsoft Visual Studio Code and paste the module code into eks-cluster.tf. 

        module "eks" {
          source  = "terraform-aws-modules/eks/aws"
          version = "19.17.2"
        }

3.	In the “eks” module block, after the version attribute, enter a couple carriage returns. Add an attribute to specify the cluster name. <b>This must match the cluster name specified in Step 16 of the vpc.tf section.</b>

        cluster_name = "myapp-eks-cluster"

4.	Refer to the following link for the latest Kubernetes versions: https://docs.aws.amazon.com/eks/latest/userguide/kubernetes-versions.html. Per the screenshot below, the latest version 1.28 will be used for the EKS cluster.  

    ![1ptce](img/prelim/tf_config/eks/1.png) 
 
    Restore eks-cluster.tf in Microsoft Visual Studio Code. Under the cluster_name attribute, add another attribute called cluster_version and set the value to the latest version:

        cluster_version = "1.28"

5.	Under the cluster_version attribute, add another attribute called cluster_endpoint_public_access and set the value to true. This allows external access to the EKS cluster.

        cluster_endpoint_public_access = true

6.	Per official Terraform documentation, notice subnet_ids and vpc_id are exposed outputs for the vpc module: https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/latest?tab=outputs 

    With this information in mind, to ensure the EKS cluster uses the VPC and subnets created in vpc.tf, the subnet_ids and vpc_id attributes will be used as outputs from the myapp-vpc module.

    Under the cluster_endpoint_public_access attribute, enter a couple carriage returns. Add two additional attributes for subnet_ids and vpc_id.

    Use the following syntax to reference the “myapp-vpc” module:<br> 
<i>module.module-name.attribute</i>

    The additional attributes and their associated values are summarized as follows:

        subnet_ids = module.myapp-vpc.private_subnets
        vpc_id = module.myapp-vpc.vpc_id

7.	Next, add a tags block to specify the environment as development and the application as “myapp.”

        tags = {
          environment = "development"
          application = "myapp"
        }

8.	Revisit the official Terraform documentation on the EKS module: https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/latest. On the browser page, press Ctrl+F and search for eks_managed_node. Copy and paste the eks_managed_node_groups block to the bottom of the eks-cluster.tf file.

        eks_managed_node_groups = {
            blue = {}
            green = {
              min_size     = 1
              max_size     = 10
              desired_size = 1

              instance_types = ["t3.large"]
              capacity_type  = "SPOT"
            }
        }

9. In the eks_managed_node_groups block, delete the following lines of code:

        blue = {}
        capacity_type = "SPOT"

10.	Finally, in the eks_managed_node_groups block, rename the green block as “dev” to correspond to the cluster being deployed in the development environment. Change the min_size value to 1, and change the max_size and desired_size to 3. Change the instance_types attribute to t2.small. These changes are summarized below:

        eks_managed_node_groups = {
          dev = {
              min_size      = 1
              max_size      = 3
              desired_size = 3

              instance_types = ["t2.small"]
          }
        }

The eks-cluster.tf Terraform configuration file is complete.

## II. Terraform: EKS Cluster Creation <a name="eks_cluster_creation">
With Terraform installed in Windows and the Terraform configuration files complete, the AWS VPC and EKS cluster can now be automatically provisioned using the <b>terraform</b> command.
1. In the top left corner of Microsoft Visual Studio, navigate to Terminal > New Terminal.

    ![1t](img/tf_eks_cluster/1.png) 
 
2. Confirm Terraform is readily availably inside the new terminal window by issuing the following command:

        terraform -v

    The output will display the version of Terraform installed.

    ![2t](img/tf_eks_cluster/2.png) 

    If an error displays that the command is not found, refer to section I.A of this project walkthrough. Microsoft Visual Studio Code may need to be closed and reopened for Terraform to be available.

3. Change to the terraform directory:

        cd terraform

4. Issue the following command to initialize the myapp-vpc and eks modules, as well as the AWS provider.

        terraform init

    After a moment, a message in green text indicating a successful initialization will appear:

    ![3t](img/tf_eks_cluster/3.png) 
 
5. Run the following command to preview the additions to the infrastructure in AWS:

        terraform plan

    The end of the output will display that 55 resources will be created:

    ![4t](img/tf_eks_cluster/4.png) 

6. Run the following command to add the 55 resources to AWS. The -auto-approve flag eliminates the need to manually enter yes to confirm the changes. This will take about 15 minutes to complete.

        terraform apply -auto-approve

7. Login to the AWS Management Console. In the search bar, enter eks and click on <b>Elastic Kubernetes Service</b>.

    ![5t](img/tf_eks_cluster/5.png) 
 
8. On the Amazon Elastic Kubernetes Service screen, confirm the myapp-eks-cluster exists.

    ![6t](img/tf_eks_cluster/6.png)  

9. In the search bar, enter ec2 as a search term and click on <b>EC2</b>.

    ![7t](img/tf_eks_cluster/7.png) 
 
10. On the EC2 management page, click Instances in the left pane. Confirm the 3 t2.small instances exist.

    ![8t](img/tf_eks_cluster/8.png) 
 
11. In the search bar, enter vpc as a search term and click on VPC.
 
    ![9t](img/tf_eks_cluster/9.png) 

12. On the VPC management screen, click Your VPCs and confirm the myapp-vpc exists.

    ![10t](img/tf_eks_cluster/10.png) 

13. Still on the VPC management screen, click on Subnets in the left pane. Confirm the 3 private and 3 public subnets exist.

    ![11t](img/tf_eks_cluster/11.png)  

The AWS VPC and EKS cluster are successfully provisioned and validation is complete.
## III. Create EKS Cluster Namespace <a name="namespace">
Before the EKS cluster namespace can be created in the Ansible playbook, Ansible requires WSL (Windows Subsystem for Linux) to be enabled in a Windows local host environment. One of the prerequisites for using the official Ansible Kubernetes module requires Python3; the pip3 command that is bundled with the Python installation will be used to install the jsonpatch, PyYaml, and Kubernetes packages, which are additional prerequisites required for the Ansible Kubernetes module. Further, the Ansible playbook will have the kubeconfig file specified as an attribute. Since the kubeconfig file leverages the aws-iam-authenticator command line tool, aws-iam-authenticator will also be installed inside WSL. This section will conclude with the installation of Ansible inside WSL.

## A) Ansible Prerequisites: <a name="ansible_prereq">

Per official Ansible documentation, Ansible is only supported on Windows with the WSL (Windows Subsystem for Linux) feature enabled (https://docs.ansible.com/ansible/latest/os_guide/windows_faq.html#windows-faq-ansible). All other scenarios with Windows are not natively supported as shown in the screenshot below. 

![1nap](img/namespace/ansible_prereq/1.png)
 
Therefore, the prerequisites outlined in this section will be installed on Windows 11 WSL (Ubuntu).

## i) Windows 11 WSL Setup <a name="wsl">

1.	To ensure WSL is enabled, use Windows Search and enter “feature” as a search criterion. Under the Control Panel listing, click “Turn Windows features on or off.”
 
    ![1napw](img/namespace/ansible_prereq/wsl/1.png)

2. On the Windows Feature dialog box, ensure Windows System for Linux is selected. If not, select it and proceed with the onscreen prompts to complete the installation.

    ![2napw](img/namespace/ansible_prereq/wsl/2.png) 

3.	With WSL enabled, an Ubuntu shell will become available for use. Open the Terminal app and click on the arrow dropdown. Click on Ubuntu:

    ![3napw](img/namespace/ansible_prereq/wsl/3.png) 

4.	<ins>Note</ins>: The root user prompt will display upon initial WSL setup. As a best practice, it is advised to create a new user and add them to the sudo group. This can be done with the following commands:

        adduser <user>
        usermod -aG sudo <user>

    Per this article, use the VIM editor to add the newly created user to the /etc/wsl.conf file (https://superuser.com/questions/1566022/how-to-set-default-user-for-manually-installed-wsl-distro). In PowerShell, execute the <b>wsl –terminate Ubuntu</b> command. From now on, each subsequent launch of WSL/Ubuntu will default as the new user and not root as shown below.

    ![4napw](img/namespace/ansible_prereq/wsl/4.png)
 
The Ansible prerequisites can now be installed inside Windows 11 WSL (Ubuntu).

## ii) Python 3: jsonpath, PyYAML, and k8s <a name="python">
1.	Navigate to the official Ansible documentation on the Kubernetes module found here: https://docs.ansible.com/ansible/latest/collections/kubernetes/core/k8s_module.html. Scroll down to the requirements section and notice the following:

    ![1napp](img/namespace/ansible_prereq/python/1.png)
 
2.	In the WSL (Ubuntu) window, update the list of packages in the apt repository.

        sudo apt update

3.	Install Python3 with the following command:

        sudo apt install python3

4.	Install pip3. Type Y and press Enter to confirm.

        sudo apt install python3-pip

5.	Navigate to the official PyPi site for the jsonpatch package (https://pypi.org/project/jsonpatch/). Copy the <b>pip</b> command to the clipboard. Paste the command from the clipboard, add the --user flag to install in the user context, and press Enter.

        pip3 install jsonpatch --user

6.	Navigate to the official PyPi site for the PyYAML package (https://pypi.org/project/PyYAML/). Copy the <b>pip</b> command to the clipboard. Paste the command from the clipboard, add the --user flag to install in the user context, and press Enter.

        pip3 install PyYAML --user

7.	Run the following two commands to compile the PyYAML and jsonpatch packages. If no errors are returned, assume these prerequisites are met.

        python3 -c "import yaml"
        python3 -c "import jsonpatch"

    ![2napp](img/namespace/ansible_prereq/python/2.png) 

    <ins>Note</ins>: As of this writing, the openshift package is no longer a prerequisite and is intentionally omitted from the project walkthrough.

8.	Last, install the Kubernetes package using pip3 under the user context:

        pip3 install kubernetes --user

The Python-related prerequisites are now met.

## iii) aws-iam-authenticator <a name="aws_iam">

Prior to creating a local copy of the kubeconfig file, the aws-iam-authenticator command line tool must be installed on Windows 11 WSL. 

1.	Navigate to the official AWS documentation for installing the aws-iam-authenticator command line tool: https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html. Click on the Linux Tab. 

2.	Use the curl command to download the binary located on GitHub. This project walkthrough assumes the local workstation uses the amd64 architecture, but a separate command exists for arm64. Copy the appropriate curl command to the clipboard.

3.	Restore the Windows 11 WSL/Ubuntu shell. Paste in the curl command and press Enter.

        curl -Lo aws-iam-authenticator https://github.com/kubernetes-sigs/aws-iam-authenticator/releases/download/v0.6.11/aws-iam-authenticator_0.6.11_linux_amd64

4. Add execute permissions to the binary folder.

        chmod +x ./aws-iam-authenticator

5. Create a parent (-p) directory located at /home/user/bin. Copy the binary to /home/user/bin/aws-iam-authenticator. Export and add /home/user/bin to the PATH variable so the aws-iam-authenticator command can be used globally.

        mkdir -p $HOME/bin && cp ./aws-iam-authenticator $HOME/bin/aws-iam-authenticator && export PATH=$HOME/bin:$PATH
        
        echo 'export PATH=$HOME/bin:$PATH' >> ~/.bashrc

6. Confirm the installation is successful by entering the following command:

        aws-iam-authenticator version
 
    ![1napa1](img/namespace/ansible_prereq/aws-iam-authenticator/1.png)

## iv) AWS CLI and aws configure <a name="awscli">   
The AWS CLI tool must be installed in WSL, since the Kubernetes module for Ansible uses the AWS Access Key ID and Secret Access Key to authenticate with the AWS credentials provider. 
1.	Navigate to the official AWS documentation for installing the AWS CLI tool (https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html). This project walkthrough assumes a x86_64 bit architecture, but the ARM commands are available on the website. 

    Expand the Linux dropdown and scroll to the Linux x86 (64-bit) tab. Copy the curl command to the clipboard to download the AWS CLI tool as a zip file.

2.	Restore the WSL/Ubuntu shell. Paste the curl command from step 1 to the terminal window.

        curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"

3.	Run the following command to install the unzip package:

        sudo apt install unzip

4.	Unzip the AWS CLI archive:

        unzip awscliv2.zip

5.	Run the AWS CLI installer:

        sudo ./aws/install

    Output will display indicating the AWS CLI command line tool is ready to use

    ![1napa2](img/namespace/ansible_prereq/awscli/1.png)
 
6. <ins>Note</ins>: This project walkthrough assumes the engineer is in possession of the Access key to their AWS account (e.g. AWS Access Key ID and Secret Access Key). If necessary, create a new user on the AWS IAM management page. Once the user is created, the Access keys section under the Security credentials tab will allow new keys to be created. Make sure to save these keys in a safe location!

    In the WSL window, enter the following command:

        aws configure

    Enter the AWS Access Key ID, AWS Secret Access Key, default region name (e.g. us-east-2), and default output format (e.g. json) as shown below:

    ![2napa2](img/namespace/ansible_prereq/awscli/2.png)

The AWS CLI tool is now installed in WSL, and the AWS credentials for programmatic access is now configured.
## v) Install Ansible in WSL <a name="install_ansible">
The section covers the installation of Ansible inside WSL.

1.	Navigate to the official Ansible documentation on installing Ansible for Ubuntu (https://docs.ansible.com/ansible/latest/installation_guide/installation_distros.html).  Scroll down to the “Installing Ansible on Ubuntu” section.

2.	As the list of packages in the apt repository are already updated from the Python section ii) above, this step can be skipped: 

        sudo apt update

3.	Install the software-properties-common package to be able to manage Personal Package Archives (PPAs). Enter Y to confirm when prompted:

        sudo apt install software-properties-common

4.	Add the Ansible PPA to the apt list of packages:

        sudo add-apt-repository –yes –update ppa:ansible/ansible

5.	Install Ansible using apt. Enter Y to confirm when prompted:

        sudo apt install ansible

6.	Check Ansible is readily available for use by entering the following command:

        ansible --version

    The output will display verison information:

    ![1napi](img/namespace/ansible_prereq/install_ansible/1.png)

Ansible is now installed inside WSL. Close out of Windows WSL/Ubuntu and switch back to native Windows.
## B) kubeconfig <a name="kubeconfig">
With the aws-iam-authenticator command tool available in WSL, a local copy of the kubeconfig file can now be created in Microsoft Visual Studio Code. The kubeconfig file is necessary because it will be referenced later in the Ansible playbook.

1.	Navigate to the official AWS documentation for creating a kubeconfig file for EKS: https://docs.aws.amazon.com/eks/latest/userguide/create-kubeconfig.html. 

2.	From the AWS website, click on the AWS IAM Authenticator for Kubernetes tab and copy the entire #!/bin/bash code to the clipboard:

        #!/bin/bash
        read -r -d '' KUBECONFIG <<EOF
        apiVersion: v1
        clusters:
        - cluster:
            server: $cluster_endpoint
            certificate-authority-data: $certificate_data
          name: arn:aws:eks:$region_code:$account_id:cluster/$cluster_name
        contexts:
        - context:
            cluster: arn:aws:eks:$region_code:$account_id:cluster/$cluster_name
            user: arn:aws:eks:$region_code:$account_id:cluster/$cluster_name
          name: arn:aws:eks:$region_code:$account_id:cluster/$cluster_name
        current-context: arn:aws:eks:$region_code:$account_id:cluster/$cluster_name
        kind: Config
        preferences: {}
        users:
        - name: arn:aws:eks:$region_code:$account_id:cluster/$cluster_name
          user:
            exec:
              apiVersion: client.authentication.k8s.io/v1beta1
              command: aws-iam-authenticator
              args:
                - "token"
                - "-i"
                - "$cluster_name"
                # - "--role"
                # - "arn:aws:iam::$account_id:role/my-role"
              # env:
                # - name: "AWS_PROFILE"
                #   value: "aws-profile"
        EOF
        echo "${KUBECONFIG}" > ~/.kube/config

3. Restore the automate-k8s-eks-tf-ansible project in Microsoft Visual Studio Code. At the root of the project folder, click the icon to create a new file. Name the file as kubeconfig_myapp-eks-cluster.

    ![1nk](img/namespace/kubeconfig/1.png) 

4.	Paste the contents from the clipboard into the kubeconfig_myapp-eks-cluster file.

5.	In the kubeconfig_myapp-eks-cluster file, delete the following lines of code:

        #!/bin/bash
        read -r -d '' KUBECONFIG <<EOF

6.	Highlight the preferences attribute and cut it to the clipboard. Paste the preferences attribute as the second line of code in the file, as shown here:

        apiVersion: v1
        preferences: {}

7.	As done in the previous step, move the kind attribute as the third line of code in the file, as shown below:

        apiVersion: v1
        preferences: {}
        kind: Config

    Enter a carriage return to create space between the kind and clusters attributes. Minimize Microsoft Visual Studio Code.

8.	Login to the AWS Management Console and navigate to the EKS management page. Click on the myapp-eks-cluster to reveal more settings.

    ![2nk](img/namespace/kubeconfig/2.png)  

9.	On the Overview Tab, locate the Details section. Copy the API server endpoint link to the clipboard. Minimize the AWS Management Console.

10.	Restore Microsoft Visual Studio Code. In the kubeconfig_myapp-eks-cluster file, replace the server attribute value by pasting the contents from the clipboard.

    ![3nk](img/namespace/kubeconfig/3.png) 
 
    Minimize Microsoft Visual Studio Code.

11.	Restore the AWS Management Console. Back in the Details section of the Overview Tab, copy the Certificate authority value to the clipboard. Minimize the AWS Management Console.

12.	Restore Microsoft Visual Studio Code. In the kubeconfig_myapp-eks-cluster file, replace the certificate-authority-data attribute value by pasting the contents from the clipboard.

    ![4nk](img/namespace/kubeconfig/4.png)  

13.	Replace myapp-eks-cluster as the value for all of the following attributes:<br>
clusters > name<br>
contexts > context > cluster<br>
contexts > context > user<br>
contexts > name<br>
current-context<br>
users > name

14.	For the args attribute, replace “$cluster_name” with the value of “myapp-eks-cluster.”

15.	Delete the remainder of lines in the kubeconfig_myapp-eks-cluster file. This includes deleting all the commented lines, as well as deleting the EOF and echo commands.

16.	The kubeconfig file is now complete. Below is a summary of changes:

        apiVersion: v1
        preferences: {}
        kind: Config

        clusters:
        - cluster:
            server: https://xxx.gr7.us-east-2.eks.amazonaws.com
            certificate-authority-data: xxxx
          name: myapp-eks-cluster

        contexts:
        - context:
            cluster: myapp-eks-cluster
            user: myapp-eks-cluster
          name: myapp-eks-cluster

        current-context: myapp-eks-cluster

        users:
        - name: myapp-eks-cluster
          user:
            exec:
              apiVersion: client.authentication.k8s.io/v1beta1
              command: aws-iam-authenticator
              args:
                - "token"
                - "-i"
                - "myapp-eks-cluster"

## C) Ansible: <a name="ansible">
This section creates Ansible configuration files and executes a play that adds a namespace to the AWS EKS Kubernetes cluster.


Restore the automate-k8s-eks-tf-ansible project in Microsoft Visual Studio Code. At the root of the project folder, create 3 new files: (1) ansible.cfg, (2) hosts, and (3) deploy-to-k8s.yaml.

![1na](img/namespace/ansible/1.png) 

## i) ansible.cfg and hosts <a name="ansiblecfg_hosts">
This section will configure the ansible.cfg and hosts files.

1.	Add the following code to the ansible.cfg file:

        [defaults]
        host_key_checking = False
        inventory = hosts

        interpreter_python = /usr/bin/python3

        remote_user = ec2-user

    Setting the host_key_checking value to False will bypass adding the host key the first time an SSH session is established.

    Setting the inventory value to hosts tells Ansibles to look at the hosts file for dynamic inventory information. Dynamic inventory is not applicable to this project.

    The interpreter_python value indicates the location of the Python3 installation.

    The remote_user value is the default ec2-user whenever an EC2 instance is created.

2.	Leave the hosts file empty. The localhost, or local workstation, will be used in this project.    
          
## ii) deploy-to-k8s.yaml: Create Namespace <a name="deploy_to_k8s">

In this section, an Ansible play will create a namespace called “my-app” in the EKS cluster.

1.	First, navigate to the official Ansible documentation for the Kubernetes module: https://docs.ansible.com/ansible/latest/collections/kubernetes/core/k8s_module.html. Scroll to the Examples section towards the bottom of the page.

2.	In the Examples section, copy to the first block of code to the clipboard:

        - name: Create a k8s namespace
          kubernetes.core.k8s:
            name: testing
            api_version: v1
            kind: Namespace
            state: present

3.	Restore the automate-k8s-eks-tf-ansible project in Microsoft Visual Studio Code. As the first line of code in the deploy-to-k8s.yaml file, enter 3 hyphens to indicate this is the beginning of the Ansible playbook. 

        ---

4.	As the second line of code in deploy-to-k8s.yaml, name the play to indicate deploying an app in the new namespace.

        ---
        - name: Deploy app in new namespace

5.	Under the name key-value pair, add a tasks section at the same spacing level as the “n” in name. Do not include a hypen.

        ---
        - name: Deploy app in new namespace
          tasks:

6.	Under the tasks section, paste the contents of the clipboard from step 2. While the pasted contents are still highlighted, indent the selection twice.

        ---
        - name: Deploy app in new namespace
          tasks:
            - name: Create a k8s namespace
              kubernetes.core.k8s:
                name: testing
                api_version: v1
                kind: Namespace
                state: present

7.	Change the name attribute value from “testing” to “my-app.”

        name: my-app

8.	Enter a blank line between the name and tasks attributes. On the new blank line, add a hosts attribute so that hosts is the same spacing level as the “n” in name above it and “t” in tasks below it. Specify the hosts value as localhost to tell the Ansible playbook the configuration will be applied to the local workstation.

        ---
        - name: Deploy app in new namespace
          hosts: localhost
          tasks:

9.	Refer back to the official Ansible documentation on the Kubernetes module: https://docs.ansible.com/ansible/latest/collections/kubernetes/core/k8s_module.html. Notice that kubeconfig is an available parameter.

    Underneath the state attribute, add the kubeconfig attribute. The kubeconfig file created as a prerequisite resides in the same root project folder as the deploy-to-k8.yaml file. Therefore, set the kubeconfig attribute with a value of the kubeconfig file name as shown below:

        kubeconfig: kubeconfig_myapp-eks-cluster

The namespace Ansible play is ready for execution.
     
## iii) Execute Playbook & Confirm Namespace <a name="execute1">
The Remote Explorer tool in Microsoft Visual Studio will allow access to WSL and the Ansible command. From Ubuntu, the /mnt directory will be leveraged to connect to the C:\ drive in Windows so that the files created in Windows can be seen on the Linux side.
1.	In Windows, open Microsoft Visual Studio Code.

2.	In the left pane, click the Remote Explorer button.

    ![1nae](img/namespace/ansible/execute/1.png)  

3.	From the WSL Targets dropdown, under Ubuntu, click the -> icon to connect in the current window:

    ![2nae](img/namespace/ansible/execute/2.png)  
 
4.	Click Open Folder.

    ![3nae](img/namespace/ansible/execute/3.png)   

5.	In the Open Folder search bar, begin by typing /mnt/c. Notice the automate-k8s-eks-tf-ansible project is present. Select the folder and click OK.

    ![4nae](img/namespace/ansible/execute/4.png)   

6.	When prompted, click <b>Yes, I trust the authors</b>.

    ![5nae](img/namespace/ansible/execute/5.png)  
 
7.	Notice the entire project created from Windows is now available on the WSL/Ubuntu side.

    ![6nae](img/namespace/ansible/execute/6.png)  
 
8.	At the File menu, click Terminal > New Terminal.

    ![7nae](img/namespace/ansible/execute/7.png)   

9.	In the Linux terminal window, enter the <b>ls</b> command and notice all the files created from the Windows side are now available at the Linux command line via WSL/Ubuntu:

    ![8nae](img/namespace/ansible/execute/8.png)   

10.	Export the ANSIBLE_CONFIG variable to the location on the Windows C Drive. This provides a workaround for the error in output stating the ansible.cfg file is in a world writable location:

        export ANSIBLE_CONFIG=/mnt/c/automate-k8s-eks-tf-ansible/ansible.cfg

11.	Inside the Ubuntu terminal in Microsoft Visual Studio Code, execute the following command to run the playbook:

        ansible-playbook deploy-to-k8s.yaml

    Ignore the warning, as while the hosts file is empty, only the localhost is being using in this project. The Ansible output displays indicating the changes were made:

    ![9nae](img/namespace/ansible/execute/9.png)   

12.	On the Windows side, open PowerShell and set the KUBECONFIG environment variable to kubeconfig_myapp-eks-cluster from the project folder:

        $env:KUBECONFIG = "C:\automate-k8s-eks-tf-ansible\kubeconfig_myapp-eks-cluster"

13.	<ins>Note</ins>: This project walkthrough assumes that kubectl is already installed on the Windows workstation. For installation instructions, navigate to the official documentation here: https://kubernetes.io/docs/tasks/tools/ 

    Check that the my-app namespace was created by viewing the output of the following command:

        kubectl get ns

    ![10nae](img/namespace/ansible/execute/10.png)  
 
## IV. Deploy nginx Application in my-app Namespace <a name="nginx">
Before writing the next play in the Ansible playbook, a Kubernetes configuration file for a nginx application must first be created. The Kubernetes configuration file will contain both a deployment and service component. 

## A) nginx Kubernetes Configuration <a name="nginx_k8s">

1.	Open Microsoft Visual Studio Code. The WSL remote session will still be connected to the mounted C:\ volume in Windows. At the root of the project folder, create a new file called nginx-config.yaml.
 
    ![1nkc](img/nginx/k8s_config/1.png)  

2.	Copy and paste the following Kubernetes configuration into nginx-config.yaml. Save the file.

        ---
        apiVersion: apps/v1
        kind: Deployment
        metadata:
          name: nginx
        spec:
          selector:
            matchLabels:
              app: nginx
          replicas: 1
          template:
            metadata:
              labels:
                app: nginx
            spec:
              containers:
                - name: nginx
                  image: nginx
                  ports:
                  - containerPort: 80
        ---
        apiVersion: v1
        kind: Service
        metadata:
          name: nginx
          labels:
            app: nginx 
        spec:
          ports:
          - name: http
            port: 80
            protocol: TCP
            targetPort: 80
          selector:
            app: nginx
          type: LoadBalancer

The Kubernetes configuration file is ready to be used in Ansible.

## B) Execute Ansible Playbook & Confirm nginx <a name="execute2">

1.	Restore the deploy-to-k8s.yaml file in Microsoft Visual Studio Code. The WSL remote session will still be connected, indicated by the Linux terminal prompt.

2.	At the same indentation level as the name attribute for the namespace play, create a new Ansible play called “Deploy nginx app.”

        - name: Deploy nginx app

3.	Specify the kubernetes module in Ansible as done in the namespace play.

        kubernetes.core.k8s:

4.	In the kubernetes.core.k8s section, specify the src attribute with a value as the Kubernetes configuration file created in section A.

        src: nginx-config.yaml

5.	Beneath the src attribute, specify the state attribute as present so that Ansible will add the changes:

        state: present

6.	Beneath the state attribute, specify the kubeconfig attribute with a value as the kubeconfig file created earlier in this project walkthrough.

        kubeconfig: kubeconfig_myapp-eks-cluster

7.	Specify the namespace as the my-app namespace created earlier in this project walkthrough:

        namespace: my-app

8.	Execute the play by issuing the following command in the WSL terminal pane inside Microsoft Visual Studio Code.

        ansible-playbook deploy-to-k8s.yaml

    The output on the WSL terminal pane will show the new “Deploy nginx app” play created in this section with a changed status.

    ![1ne](img/nginx/execute/1.png)  

9. In native Windows, open PowerShell. Export the kubeconfig variable as done previously:

        $env:KUBECONFIG = "C:\automate-k8s-eks-tf-ansible\kubeconfig_myapp-eks-cluster"

10.	Check the status of the nginx pod and LoadBalancer service in the my-app namespace with the following commands:

        kubectl get pod -n my-app
        kubectl get svc -n my-app

    The output will show the nginx pod is in a running state, as well as the DNS name associate with the LoadBalancer service.

    ![2ne](img/nginx/execute/2.png) 

11.	Copy the DNS name of the nginx application to the clipboard. Open a web browser, paste the DNS name, and press Enter. This may take a moment but should load.

    ![3ne](img/nginx/execute/3.png) 

## C) Optimization: kubeconfig  <a name="optimize">
In the deploy-to-k8s.yaml Ansible playbook, recall the kubeconfig attribute is specified in both the namespace and deploying nginx plays. This section optimizes the Ansible playbook by setting the kubeconfig value once as an environment variable, so that it does not need to be specified in each play.

1.	In a web browser, navigate to the official Ansible documentation for the Kubernetes module: https://docs.ansible.com/ansible/latest/collections/kubernetes/core/k8s_module.html. Use Ctrl+F to find the kubeconfig attribute.

2.	Notice the kubeconfig attribute indicates the kubeconfig file can be specified with the K8S_AUTH_KUBECONFIG environment variable. Copy the environment variable to the clipboard.

    ![1no](img/nginx/optimize/1.png) 

3.	Restore Microsoft Visual Studio Code. The WSL connection with the Linux shell will still be open. In the Linux prompt, use the export command to set the K8S_AUTH_KUBECONFIG environment variable to the Windows path of the kubeconfig file:

        export K8S_AUTH_KUBECONFIG=/mnt/c/automate-k8s-eks-tf-ansible/kubeconfig_myapp-eks-cluster

4.	In deploy-to-k8s.yaml, delete the kubeconfig attributes from both plays. Save the file.

5.	Rerun the Ansible playbook and notice the results are successful.
 
    ![2no](img/nginx/optimize/2.png)

## V. Project Teardown <a name="teardown">

To not incur any additional charges associated with the AWS EKS cluster and AWS VPC, all resources created with Terraform in this project will now be deleted.

1.	Close out of the WSL/Ubuntu Remote Explorer window in the automate-k8s-eks-tf-ansible project. Open the project locally on the Windows side through the C:\ drive.

2.	Open a terminal window and a PowerShell prompt will display.


3.	Change to the terraform directory with the following command:

        cd terraform

    ![1t](img/teardown/1.png)

4.	Run the following command to delete all EKS/VPC resources from AWS. Use the -auto-approve flag to not have to manually enter yes for confirmation.

        terraform destroy -auto-approve

    Output will display to show the progress of the AWS resource removal. This will take about 20 minutes to complete.

    ![2t](img/teardown/2.png) 

5.	A warning and 3 errors pertaining to VPC resources display. These errors pertain to VPC dependencies that are preventing removal:
 
    ![3t](img/teardown/3.png)
    ![4t](img/teardown/4.png)
 
6.	Login to the AWS Management Console and navigate to the EC2 management page. Find the one load balancer that exists and manually delete it from AWS management portal.
 
    ![5t](img/teardown/5.png)

7.	On the Delete load balancer dialog box, type confirm and click the <b>Delete</b> button.

    ![6t](img/teardown/6.png)
 
8.	A message will display indicating the load balancer is successfully deleted.

    ![7t](img/teardown/7.png) 

9.	If the <b>terraform destroy -auto-approve</b> command is rerun in the terminal, additional VPC dependencies exists. First, find out the VpcID attribute by issuing the following command:

        aws ec2 describe-vpcs

10.	Launch a new WSL/Ubuntu window in the Terminal app. In the user’s home directory, create a file called script.sh and apply executable permission:

        touch script.sh
        chmod +x script.sh

11.	Open the VIM editor and paste the following script inside script.sh. Replace the vpc with the VpcID attribute associated with the myapp-vpc VPC from step 9. The script comes from the following link: https://repost.aws/knowledge-center/troubleshoot-dependency-error-delete-vpc

    Save the changes by entering command mode and typing :wq

        #!/bin/bash
        vpc="xxx"
        region="us-east-2"
        aws ec2 describe-internet-gateways --region $region --filters 'Name=attachment.vpc-id,Values='$vpc | grep InternetGatewayId
        aws ec2 describe-subnets --region $region --filters 'Name=vpc-id,Values='$vpc | grep SubnetId
        aws ec2 describe-route-tables --region $region --filters 'Name=vpc-id,Values='$vpc | grep RouteTableId
        aws ec2 describe-network-acls --region $region --filters 'Name=vpc-id,Values='$vpc | grep NetworkAclId
        aws ec2 describe-vpc-peering-connections --region $region --filters 'Name=requester-vpc-info.vpc-id,Values='$vpc | grep VpcPeeringConnectionId
        aws ec2 describe-vpc-endpoints --region $region --filters 'Name=vpc-id,Values='$vpc | grep VpcEndpointId
        aws ec2 describe-nat-gateways --region $region --filter 'Name=vpc-id,Values='$vpc | grep NatGatewayId
        aws ec2 describe-security-groups --region $region --filters 'Name=vpc-id,Values='$vpc | grep GroupId
        aws ec2 describe-instances --region $region --filters 'Name=vpc-id,Values='$vpc | grep InstanceId
        aws ec2 describe-vpn-connections --region $region --filters 'Name=vpc-id,Values='$vpc | grep VpnConnectionIdaws ec2 describe-vpn-gateways --region $region --filters 'Name=attachment.vpc-id,Values='$vpc | grep VpnGatewayId
        aws ec2 describe-network-interfaces --region $region --filters 'Name=vpc-id,Values='$vpc | grep NetworkInterfaceId
        aws ec2 describe-carrier-gateways --region $region --filters Name=vpc-id,Values=$vpc | grep CarrierGatewayIdaws ec2 describe-local-gateway-route-table-vpc-associations --region $region --filters Name=vpc-id,Values=$vpc | grep LocalGatewayRouteTableVpcAssociationId

12.	Back on the Linux command line, execute the script by entering the following command:

        ./script.sh

    Observe the output results from the script. As the route table is explicitly created from the vpc.tf file, deductive reasoning is used in determining the route table is not the dependency preventing removal. 

    ![8t](img/teardown/8.png) 

13.	Issue the following command to distinguish the default security group for the security group associated with myapp-vpc:

        aws ec2 describe-security-groups

14.	Issue the following command to manually delete the security group associated with myapp-vpc.

        aws ec2 delete-security-group --group-id <group id>

15.	Open PowerShell and change to the terraform directory of the project folder. Rerun the following command:

        terraform destroy -auto-approve

    ![9t](img/teardown/9.png)  

    The command is now successful.

<ins>Validation:</ins><br>
1. Restore the AWS Management Console and navigate to the EKS management page. Confirm that no clusters exist.

    ![10t](img/teardown/10.png) 
 
2.	In the AWS Management portal, navigate to the VPC dashboard and confirm all defaults are restored:

    ![11t](img/teardown/11.png) 

3. Finally, in the AWS Management portal, navigate to the EC2 Dashboard and confirm no instances are running. Also confirm the load balancers and security group are no longer present.

    ![12t](img/teardown/12.png)  

The project cleanup is complete; there will be no further AWS charges.
